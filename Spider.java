
/*Created by Philipp Bürki based mainly on tutorial from http://www.netinstructions.com/how-to-make-a-simple-web-crawler-in-java/*/

/* This program will search one website and its subdirectories for a word and returns the result as a txt file. */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;

public class Spider
{
  private static final int MAX_PAGES_TO_SEARCH = 10;
  private Set<String> pagesVisited = new HashSet<String>();
  private List<String> pagesToVisit = new LinkedList<String>();


  /**
   * Our main launching point for the Spider's functionality. Internally it creates spider legs
   * that make an HTTP request and parse the response (the web page).
   * 
   * @param url
   *            - The starting point of the spider
   * @param searchWord
   *            - The word or string that you are searching for
   */
  public void search(String url, String searchWord)
  {
	  int Counter = 0; 
      while(this.pagesVisited.size() < MAX_PAGES_TO_SEARCH)
      {
          String currentUrl;
          SpiderLeg leg = new SpiderLeg();
          if(this.pagesToVisit.isEmpty())
          {
              currentUrl = url;
              this.pagesVisited.add(url);
          }
          else
          {
              currentUrl = this.nextUrl();
          }
          leg.crawl(currentUrl); // Lots of stuff happening here. Look at the crawl method in
                                 // SpiderLeg
          boolean success = leg.searchForWord(searchWord);
          if(success)
          {
              System.out.println(String.format("**Success** Word %s found at %s", searchWord, currentUrl));
              Counter++;
              
              //Write to Textfile
              try{
              	String content = String.format("**Success** Word %s found at %s", searchWord, currentUrl);
                  //Specify the file name and path here
              	File file =new File("Websites.txt");

              	/* This logic is to create the file if the
              	 * file is not already present
              	 */
              	if(!file.exists()){
              	   file.createNewFile();
              	}

              	//Here true is to append the content to file
              	FileWriter fw = new FileWriter(file,true);
              	//BufferedWriter writer give better performance
              	BufferedWriter bw = new BufferedWriter(fw);
              	bw.newLine();
              	bw.write(content);
              	//Closing BufferedWriter Stream
              	bw.close();

              	System.out.println("Data successfully appended at the end of file");

                }catch(IOException ioe){
                   System.out.println("Exception occurred:");
              	 ioe.printStackTrace();
                 }

          }
          this.pagesToVisit.addAll(leg.getLinks());
      }
      System.out.println("\n**Done** Visited " + this.pagesVisited.size() + " web page(s)");
      JOptionPane.showMessageDialog(null,String.format("**Success** Word %s found %d times", searchWord, Counter));
  }


  /**
   * Returns the next URL to visit (in the order that they were found). We also do a check to make
   * sure this method doesn't return a URL that has already been visited.
   * 
   * @return
   */
  private String nextUrl()
  {
      String nextUrl;
      do
      {
          nextUrl = this.pagesToVisit.remove(0);
      } while(this.pagesVisited.contains(nextUrl));
      this.pagesVisited.add(nextUrl);
      return nextUrl;
  }
}