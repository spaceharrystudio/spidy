/*Created by Philipp Bürki based mainly on tutorial from http://www.netinstructions.com/how-to-make-a-simple-web-crawler-in-java/*/

/* This program will search one website and its subdirectories for a word and returns the result as a txt file. */

import javax.swing.*;


public class SpiderTest
{
    /**
     * This is our test. It creates a spider (which creates spider legs) and crawls the web.
     * 
     * @param args
     *            - not used
     */
    public static void Spider(String Website, String Word)
    {
        Spider spider = new Spider();
        spider.search(Website, Word);
    }


    /**
     * JOptionPane showInputDialog example #2.
     * @author alvin alexander, http://alvinalexander.com
     */
   
      public static void main(String[] args)
      {
    	  
    	  JTextField xField = new JTextField(5);
          JTextField yField = new JTextField(5);
          
    	  JPanel myPanel = new JPanel();
    	  myPanel.setLayout(new BoxLayout(myPanel, BoxLayout.Y_AXIS));
          myPanel.add(new JLabel("Website:"));
          myPanel.add(xField);
          myPanel.add(Box.createVerticalStrut(15)); // a spacer
          myPanel.add(new JLabel("Word:"));
          myPanel.add(yField);

          int result = JOptionPane.showConfirmDialog(null, myPanel, 
                   "Please Enter Website and Word to search for", JOptionPane.OK_CANCEL_OPTION);
          if (result == JOptionPane.OK_OPTION) {
        	  
        	  Spider("http://" + xField.getText(), yField.getText());
        	  
          
      }
    


      }
}
